int max2(int a, int b) {
    if (a < b) {
        return b;
    } else {
        return a;
    }
}


int max3( int a, int b, int c) {
    return max2(max2(a, b),c);

}
void exercice1(){
    Ut.afficherSL("ecrire un nombre");
    int a = Ut.saisirEntier();
    Ut.afficherSL("ecrire un nombre");
    int b = Ut.saisirEntier();
    Ut.afficherSL("le plus grand des deux est " + max2(a,b));
    Ut.afficherSL("ecrire un nombre");
    a = Ut.saisirEntier();
    Ut.afficherSL("ecrire un nombre");
    b = Ut.saisirEntier();
    Ut.afficherSL("ecrire un nombre");
    int c = Ut.saisirEntier();
    Ut.afficherSL("le plus grand des trois est " + max3(a,b,c));


}
void repetcarac(int nb ,char car ){
        for(int i = 0 ; i < nb ; i++){
            Ut.afficher(car);
        }
}
void Pyramide(int h, char c){
   for (int i = 1; i <= h; i++) {
       repetcarac(h-i, ' ');
       repetcarac(2*i-1, c );
       Ut.sautLigne();
   }
}


void exercice3() {
    int nb1 = 0;
    int nb2 = 0;
    int i = 1;
    Ut.afficherSL("tapper un premier nombre");
    nb1 = Ut.saisirEntier();
    Ut.afficherSL("tapper un deuxieme nombre");
    nb2 = Ut.saisirEntier();

    if (nb1 > nb2) {
        Ut.afficherSL("nous ne pouvons rien faire...");
    }
    else {
        croissant(nb1, nb2);
    }
}
int croissant(int nb1,int nb2) {
    int unité = 0;
        while (nb1 != nb2) {
            unité = nb1 % 10;
            Ut.afficher(unité);
            nb1 = nb1 + 1;
        }
    return unité;
}



int decroissant(int nb1, int nb2) {
    int unité = 0;
        while (nb2 != nb1) {
            unité = nb2 % 10;
            Ut.afficher(unité);
            nb2 = nb2 - 1;
        }

    return unité;
}

void pyramidecompliqué(int h){
    int space = h-1;
    for (int i = 1; i <= h; i++) {

        repetcarac(space, ' ');
        croissant(i,2*i-1);
        decroissant(i-1,2*i-1);
        Ut.sautLigne();
        space = space - 1;

    }
}

int nbChiffre(int n){
    int resultat = 0;
    int i = 0;
    while (n > 0) {
        resultat = n % 10;
        Ut.afficherSL( resultat);
        n = n / 10;
        i = i + 1;
    }

    Ut.afficherSL("il y a "+i+" chiffre");
    return n;
}
 int nbChiffreDuCarré(int n){
    Ut.afficherSL("saisir nombre :");
    nbChiffre(n*n);
    return n;
 }
 void exo4(){
    int n = Ut.saisirEntier();
     nbChiffreDuCarré(n);
 }

 int amis(int p, int q){
    int n = 1;
    int sp = 0;
    while (n<p){
        if (p % n == 0){
            sp = sp + n;
        }
        n++;
    }
    int i = 1;
    int sq = 0;
    while (i<q){
         if (q % i == 0){
             sq = sq + i;
         }
         i++;
    }



    if ((sp == q) && (sq == p)) {
        Ut.afficherSL("les nombres "+p+" et "+q+" sont amis");
     }
    //else {
    //    Ut.afficherSL("les nombres " +p+" et "+q+" ne sont pas amis");
    //}
    return sp;

 }

 void exercice5(){
     int nb = 0;
    complexe(nb);
}
int simple() {
    Ut.afficherSL("saisir un nombre : ");
    int p = Ut.saisirEntier();
    Ut.afficherSL("saisir un nombre : ");
    int q = Ut.saisirEntier();
    return q;
}

int complexe(int nb){
    nb = Ut.saisirEntier();
    int c = 0;
    int c4 = 0;
     while (c4 <= nb) {
         if (c >= c4) {
             c = 1;
             c4++;
         }
         amis(c,c4);
         c++;
     }
     return c;
}
 int racineParfaite(int c){
    if (Math.sqrt(c) % 1 == 0){
        double n = Math.sqrt(c);
        Ut.afficherSL("le nombre "+n+" est une racine parfaite");
    }
    else {
        Ut.afficherSL("-1");
    }

     return c;
 }

int estCarreParfait(int c){
    if (Math.sqrt(c) % 1 == 0){
        Ut.afficherSL("le nombre "+c+" est un carré parfait");
    }
    else {
        Ut.afficherSL("nope, pas un carré parfait");
    }

    return c;
}

int Phytagore(int c1, int c2){
    if ( (c1 == 0) || (c2==0) ) {
        Ut.afficherSL("Erreur : un coté est nul");
    }
    else {
        estCarreParfait(c1 * c1 + c2 * c2);
        racineParfaite(c1 * c1 + c2 * c2);
    }


    return c1;
}


void exercice6(){
    Ut.afficherSL("saisir un nombre positif : ");
    int  c1 = Ut.saisirEntier();
    Ut.afficherSL("saisir un nombre positif : ");
    int c2 = Ut.saisirEntier();
      Phytagore(c1, c2);
}

int ex6_1(){
        Ut.afficherSL("saisir un nombre : ");
        int c = Ut.saisirEntier();
        estCarreParfait(c);
        return c;
}

void exercice7(){
    Ut.afficherSL("saisir un nombre : ");
    int n = Ut.saisirEntier();
    int c1 = 0;
    int c2 = 0;
    int c3 = 0;
    while ( c3 < n ){
        if ( c1 == n ){
            c1=0;
            c2++;
            if ( c2 == n ){
                c2=0;
                c3++;
            }
        }
        c1++;
        if ((c1+c2+c3 == n) && (c3 != 0)) {
            Ut.afficherSL(c1 + " + " + c2 + " + " + c3 + " = " + (c1 + c2 + c3));
            Phytagore(c1, c2);
        }
    }
}

void exercice8(){
    Syracausien_de_base(123456789);
}

int exo8_1(){
    Ut.afficherSL("saisir un nombre : ");
    int nb = Ut.saisirEntier();
    int i = 1;
    while (i <= nb){
        Ut.afficherSL("pour" +i);
        Syracausien_de_base(i);
        i++;
    }

    return nb;
}

int exo8_2(){
    Ut.afficherSL("saisir un nombre : ");
    int nb = Ut.saisirEntier();
    Syracausien_de_base(nb);

    return nb;
}

int exo8_3(){
    Ut.afficherSL("saisir un nombre : ");
    int nb = Ut.saisirEntier();
    Ut.afficherSL("saisir un nombre : ");
    int nbmxop = Ut.saisirEntier();
    for (int i = 1; i <= nbmxop; i++){
        if ( nb != 1 ) {
            if (nb % 2 != 0) {
                nb = 3 * nb + 1;
            }
            if (nb % 2 == 0) {
                nb = nb / 2;
            }
        }

        Ut.afficherSL(nb);
    }
    boolean v = true;
    if ( nb == 1 ){
        v = true;
    }
    else {
        v = false;
    }
    Ut.afficherSL(v);
    return v ? 1 : 0;
}

int Syracausien_de_base (int n) {
        while ( n != 1 ){
             if ( n % 2 != 0 ){
                 n = 3*n+1;
             }
             if ( n % 2 == 0 ){
                 n = n / 2;
             }
             Ut.afficherSL(n);
        }

        return n;
}

void main() {
    exercice8();
}
